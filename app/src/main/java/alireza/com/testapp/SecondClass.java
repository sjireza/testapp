package alireza.com.testapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondClass extends AppCompatActivity {
    TextView hello, greatings;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_class);
        hello = (TextView)findViewById(R.id.hello);
        greatings = (TextView)findViewById(R.id.greeatings);
        btn = (Button)findViewById(R.id.btn);
    }

    public void forwortoPreviousActivity(View view) {
        this.finish();
    }
}
